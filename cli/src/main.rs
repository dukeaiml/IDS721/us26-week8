use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;


// Define a struct to represent a character
#[derive(Debug, PartialEq)]
struct Character {
    brand:String,
    model:String,
    body_style:String,
    segment:String,
    seats:String,
    rapid_charge:String,
}

impl Character {
    // Function to create a new character from CSV record
    fn new(record: csv::StringRecord) -> Character {
        Character {
            brand: record.get(0).unwrap_or("").to_string(),
            model: record.get(1).unwrap_or("").to_string(),
            body_style: record.get(2).unwrap_or("").to_string(),
            segment: record.get(3).unwrap_or("").to_string(),
            seats: record.get(4).unwrap_or("").to_string(),
            rapid_charge: record.get(5).unwrap_or("").to_string(),
        }
    }
}

// Function to read characters from CSV and return vector of characters
fn read_characters_from_csv(file_path: &str) -> Result<Vec<Character>, Box<dyn Error>> {
    let mut characters = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let character = Character::new(record);
        characters.push(character);
    }
    Ok(characters)
}

// Function to find character by brand (case-sensitive)
fn find_character_by_name<'a>(characters: &'a [Character], brand: &str) -> Option<&'a Character> {
    characters.iter().find(|c| c.brand == brand)
}


fn main() {
    // Read characters from CSV
    let characters = match read_characters_from_csv("./data/ev_data.csv") {
        Ok(chars) => chars,
        Err(err) => {
            eprintln!("Error reading characters: {}", err);
            return;
        }
    };

    // Get character name from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <brand_name>", args[0]);
        return;
    }
    let brand_name = &args[1];

    // Find character by name and print information
    match find_character_by_name(&characters, brand_name) {
        Some(character) => println!("{:?}", character),
        None => println!("Character not found"),
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_character_by_name_existing() {
        let characters = vec![
            Character {
                brand: "BMW".to_string(),
                model: "iX3".to_string(),
                body_style: "SUV".to_string(),
                segment: "D".to_string(),
                seats: "5".to_string(),
                rapid_charge: "180".to_string(),
            },
            Character {
                brand: "Mercedes".to_string(),
                model: "EQA".to_string(),
                body_style: "SUV".to_string(),
                segment: "C".to_string(),
                seats: "5".to_string(),
                rapid_charge: "200".to_string(),
            },
        ];

        assert_eq!(
            find_character_by_name(&characters, "BMW"),
            Some(&Character {
                brand: "BMW".to_string(),
                model: "iX3".to_string(),
                body_style: "SUV".to_string(),
                segment: "D".to_string(),
                seats: "5".to_string(),
                rapid_charge: "180".to_string(),
            })
        );
    }

    #[test]
    fn test_find_character_by_name_not_existing() {
        let characters = vec![
            Character {
                brand: "BMW".to_string(),
                model: "iX3".to_string(),
                body_style: "SUV".to_string(),
                segment: "D".to_string(),
                seats: "5".to_string(),
                rapid_charge: "180".to_string(),
            },
            Character {
                brand: "Mercedes".to_string(),
                model: "EQA".to_string(),
                body_style: "SUV".to_string(),
                segment: "C".to_string(),
                seats: "5".to_string(),
                rapid_charge: "200".to_string(),
            },
        ];

        assert_eq!(find_character_by_name(&characters, "Mrcedes"), None);
    }

    #[test]
    fn test_find_character_by_name_case_insensitive() {
        let characters = vec![
            Character {
                brand: "BMW".to_string(),
                model: "iX3".to_string(),
                body_style: "SUV".to_string(),
                segment: "D".to_string(),
                seats: "5".to_string(),
                rapid_charge: "180".to_string(),
            },
            Character {
                brand: "Mercedes".to_string(),
                model: "EQA".to_string(),
                body_style: "SUV".to_string(),
                segment: "C".to_string(),
                seats: "5".to_string(),
                rapid_charge: "200".to_string(),
            },
        ];

        assert_eq!(
            find_character_by_name(&characters, "BMW"),
            Some(&Character {
                brand: "BMW".to_string(),
                model: "iX3".to_string(),
                body_style: "SUV".to_string(),
                segment: "D".to_string(),
                seats: "5".to_string(),
                rapid_charge: "180".to_string(),
            })
        );
    }
}