# us26 Week8


1. Create a new Rust project with the specified name:

```bash
cargo new cli
```

2. Prepare the CSV data file containing information about EV cars. Name the file `ev_data.csv` and place it in the root directory of your project.

3. Add the `csv` dependency to the `Cargo.toml` file:

```toml
[dependencies]
csv = "1.1.6"
```

4. Implement the program functionality in `main.rs` based on the requirements. You need to parse the CSV file, process the data, and implement the functionality to find cars based on their brand names.



# Project Name

This is a Rust command-line tool to search for car information.

## Data Format

The data file (`ev_data.csv`) contains information about Star Wars characters or cars. The format is as follows:

```
Brand,Model,AccelSec,TopSpeed_KmH,Range_Km,Efficiency_WhKm,FastCharge_KmH,rapid_charge,PowerTrain,PlugType,body_style,segment,seats,PriceEuro
Tesla,Model 3 Long Range Dual Motor,4.6,233,450,161,940,Yes,AWD,Type 2 CCS,Sedan,D,5,55480
```

## Building and Running

To build the program, use the following command:

```bash
cargo build
```

To run the program with a character or car name, execute:

or

```bash
./target/debug/cli <car_brand>
```

Replace `<car_brand>` with the desired name.

## Example Usage

If the character or car is found, the program will display their information. If not found, an appropriate message will be shown.

```
./target/debug/cli BMW
```

# Tests Explanation

The tests in this project ensure that the program's functionalities work as expected. Here's an explanation of each test case:

## `test_find_character_by_name_existing`

This test verifies the `find_character_by_name` function when searching for an existing character. It creates a vector of characters and asserts that the function returns `Some` with the correct character when searching for an existing brand ("BMW").

## `test_find_character_by_name_not_existing`

This test verifies the `find_character_by_name` function when searching for a non-existing character. It creates a vector of characters and asserts that the function returns `None` when searching for a brand that doesn't exist in the data ("Mrcedes").

## `test_find_character_by_name_case_insensitive`

This test verifies that the `find_character_by_name` function performs a case-insensitive search. It creates a vector of characters and asserts that the function returns the correct character even when the search term is in a different case ("BMW" vs. "bmw").

Each test case provides a specific scenario to ensure the correctness of the implemented functionality.

To run these tests, use the following command:

```bash
cargo test
```

# Output 


![alt text](cli/screenshots/1.png)

![alt text](cli/screenshots/2.png)

![alt text](cli/screenshots/3.png)

![alt text](cli/screenshots/4.png)

![alt text](cli/screenshots/5.png)

## Author

Udyan Sachdev
